#!/bin/bash

# <output> | 'label'=value[UOM];[warn];[crit];[min];[max]

OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

# param1 n1
# param2 n2
function lowest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -le $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

# param1 n1
# param2 n2
function greatest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -ge $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

function getApacheInfo
{
	local errno=0

	local deltaT=3

	local serverStatus0
	local serverStatus1

	declare -A data0
	declare -A data1

	local deltaAccess
	local deltakB
	local deltaDuration

	local uptime
	local reqPerSec
	local kBPerSec
	local kBPerReq
	local msPerReq
	local scoreboard

	if [[ $errno -eq $OK ]]
	then
		serverStatus0=$(curl -s localhost/server-status?auto)
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
		sleep $deltaT
		serverStatus1=$(curl -s localhost/server-status?auto)
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		data0["Total Accesses"]=$(grep "^Total Accesses" <<< "$serverStatus0" | tr -d " " | cut -d":" -f2)
		data0["Total kBytes"]=$(grep "^Total kBytes" <<< "$serverStatus0" | tr -d " " | cut -d":" -f2)
		data0["Total Duration"]=$(grep "^Total Duration" <<< "$serverStatus0" | tr -d " " | cut -d":" -f2)

		data1["Total Accesses"]=$(grep "^Total Accesses" <<< "$serverStatus1" | tr -d " " | cut -d":" -f2)
		data1["Total kBytes"]=$(grep "^Total kBytes" <<< "$serverStatus1" | tr -d " " | cut -d":" -f2)
		data1["Total Duration"]=$(grep "^Total Duration" <<< "$serverStatus1" | tr -d " " | cut -d":" -f2)

		deltaAccess=$((data1["Total Accesses"] - data0["Total Accesses"]))
		deltakB=$((data1["Total kBytes"] - data0["Total kBytes"]))
		deltaDuration=$((data1["Total Duration"] - data0["Total Duration"]))

		uptime=$(grep "^Uptime" <<< "$serverStatus1" | cut -d" " -f2)
		scoreboard=$(grep "^Scoreboard" <<< "$serverStatus1" | cut -d" " -f2 | sed -e 's/\.*$//g')

		reqPerSec=$((deltaAccess / deltaT))
		kBPerSec=$((deltakB / deltaT))
		kBPerReq=$((deltakB / deltaAccess))
		msPerReq=$((deltaDuration / deltaAccess))
	fi

	echo "$uptime $reqPerSec $kBPerSec $kBPerReq $msPerReq $busyWorkers $idleWorkers $scoreboard"

	return $errno
}

function main
{
	local errno=$OK
	local state=""

	local output=""
	local perfdata=""

	local uptime
	local reqPerSec
	local kBPerSec
	local kBPerReq
	local msPerReq
	local scoreboard

	if [[ $errno -eq $OK ]]
	then
		apacheInfo=$(getApacheInfo)
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		read uptime reqPerSec kBPerSec kBPerReq msPerReq scoreboard <<< "$apacheInfo"
		output="$(echo -e "#\n"\
			" uptime : ${uptime}s\n" \
			"  req/s : ${reqPerSec}\n" \
			"   kB/s : ${kBPerSec}\n" \
			" kB/req : ${kBPerReq}\n" \
			" ms/req : ${msPerReq}\n" \
			"workers : ${scoreboard}\n" \
		)"

		# <output> | 'label'=value[UOM];[warn];[crit];[min];[max]
		perfdata="$(echo \
			"'req/s'=$reqPerSec;$rpsw;$rpsc;0;$rpsc" \
			"'kB/s'=$kBPerSec;$kbpsw;$kbpsc;0;$kbpsc" \
			"'ms/req'=${msPerReq}ms;$msprw;$msprc;0;$msprc" \
		)"

		if [[ $reqPerSec -ge $rpsw ]]
		then
			if [[ $reqPerSec -ge $rpsc ]]
			then
				errno=$(greatest $errno $CRITICAL)
			else
				errno=$(greatest $errno $WARNING)
			fi
		fi
	fi

	case $errno in
		0 ) state=$(echo "OK - $state");;
		1 ) state=$(echo "WARNING - $state");;
		2 ) state=$(echo "CRITICAL - $state");;
		* ) state=$(echo "UNKNOWN - $state");;
	esac

	echo -e "$state\n$output|$perfdata"

	return $errno
}

cmd=${0##*/}
cmd=${cmd%.*}

errno=0
rpsw=150
rpsc=300
kbpsw=10000
kbpsc=50000
msprw=5000
msprc=10000

if [[ $errno -eq 0 ]]
then
	opts=$(getopt \
		--name "$cmd" \
		--options "" \
		--longoptions "rpsw:,rpsc:,kbpsw:,kbpsc:,msprw:,msprc:" \
		-- "$@"
	)
	if [[ $? -ne 0 ]]
	then
		errno=1
	fi
fi

if [[ $errno -eq 0 ]]
then
	moreopt=0
	while [[ $moreopt -eq 0 ]]
	do
		case "$1" in
			-w | --warning ) warnThreshold=$2; shift 2;;
			-c | --critical ) critThreshold=$2; shift 2;;
			-- ) moreopt=1; shift 1;;
			* ) moreopt=1;;
		esac
	done

	warnThreshold=$(lowest $warnThreshold $critThreshold)
	critThreshold=$(greatest $warnThreshold $critThreshold)

	eval set -- ${opts##*--}
	shift $((OPTIND-1))
fi

if [[ $errno -eq 0 ]]
then
	main
	errno=$?
fi

exit $errno
