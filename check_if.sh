#!/bin/bash

# <output> | 'label'=value[UOM];[warn];[crit];[min];[max]

OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

# param1 n1
# param2 n2
function lowest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -le $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

# param1 n1
# param2 n2
function greatest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -ge $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

function getIfInfo
{
	local errno=0
	local errnopipe

	local inter=3

	local m0
	local m1

	local interface
	local rBytes
	local rPackets
	local rDrop
	local tBytes
	local tPackets
	local tDrop

	declare -A rkBytesps
	declare -A rPacketsps
	declare -A rDropps
	declare -A tkBytesps
	declare -A tPacketsps
	declare -A tDropps

	if [[ $errno -eq 0 ]]
	then
		m0=$(tail -n+3 /proc/net/dev)
	fi
	if [[ $errno -eq 0 ]]
	then
		sleep $inter
		m1=$(tail -n+3 /proc/net/dev)
	fi

	if [[ $errno -eq 0 ]]
	then
		m0=$(echo "$m0" | tr -d ":" | tr "|" " " | tr -s " " | grep -o "[^[:space:]].*$" | cut -d" " -f1,2,3,5,10,11,13)
		m1=$(echo "$m1" | tr -d ":" | tr "|" " " | tr -s " " | grep -o "[^[:space:]].*$" | cut -d" " -f1,2,3,5,10,11,13)
	fi


	if [[ $errno -eq 0 ]]
	then
		while read interface rBytes rPackets rDrop tBytes tPackets tDrop
		do
			rkBytesps[$interface]=$((rBytes/1000))
			rPacketsps[$interface]=$rPackets
			rDropps[$interface]=$rDrop
			tkBytesps[$interface]=$((tBytes/1000))
			tPacketsps[$interface]=$tPackets
			tDropps[$interface]=$tDrop
		done <<< "$m1"

		while read interface rBytes rPackets rDrop tBytes tPackets tDrop
		do
			rkBytesps[$interface]=$(((rkBytesps[$interface]-rBytes/1000)/inter))
			rPacketsps[$interface]=$(((rPacketsps[$interface]-rPackets)/inter))
			rDropps[$interface]=$(((rDropps[$interface]-rDrop)/inter))
			tkBytesps[$interface]=$(((tkBytesps[$interface]-tBytes/1000)/inter))
			tPacketsps[$interface]=$(((tPacketsps[$interface]-tPackets)/inter))
			tDropps[$interface]=$(((tDropps[$interface]-tDrop)/inter))

			echo "$interface ${rkBytesps[$interface]} ${rPacketsps[$interface]} ${rDropps[$interface]} ${tkBytesps[$interface]} ${tPacketsps[$interface]} ${tDropps[$interface]}"
		done <<< "$m0"
	fi

	return $errno
}

function main
{
	local errno=$OK
	local state=""

	local output="#"
	local perfdata=""
	local ifInfo
	local ifOutput
	local ifPerfdata

	if [[ $errno -eq $OK ]]
	then
		ifInfo=$(getIfInfo)
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		output=$(echo -e "$output\n         reçu                         envoyé")
		output=$(echo -e "$output\n         koctets/s paquets/s perdus/s koctets/s paquets/s perdus/s")
		while read interface rkBytesps rPacketsps rDropps tkBytesps tPacketsps tDropps
		do
			if [[ $rDropps -ge $warnThreshold ]] || [[ $tDropps -ge $warnThreshold ]]
			then
				if [[ $rDropps -ge $critThreshold ]] || [[ $tDropps -ge $critThreshold ]]
				then
					errno=$(greatest $errno $CRITICAL)
				else
					errno=$(greatest $errno $WARNING)
				fi
			fi

			ifOutput=$(printf "%6s : %9d %9d %8d %9d %9d %8d" $interface $rkBytesps $rPacketsps $rDropps $tkBytesps $tPacketsps $tDropps)
			ifPerfdata="$(echo \
				"'${interface} kB/s down'=${rkBytesps}kb;" \
				"'${interface} drop down'=${rDropps}c;$warnThreshold;$critThreshold;0;$critThreshold" \
				"'${interface} kB/s up'=${tkBytesps}kb;" \
				"'${interface} drop up'=${tDropps}c;$warnThreshold;$critThreshold;0;$critThreshold"
			)"

			output=$(echo -e "$output\n$ifOutput")
			perfdata=$(echo "$perfdata$ifPerfdata ")
		done <<< "$ifInfo"
	fi

	case $errno in
		0 ) state=$(echo "OK - $state");;
		1 ) state=$(echo "WARNING - $state");;
		2 ) state=$(echo "CRITICAL - $state");;
		* ) state=$(echo "UNKNOWN - $state");;
	esac

	echo -e "$state\n$output|$perfdata"

	return $errno
}

cmd=${0##*/}
cmd=${cmd%.*}

errno=0
warnThreshold=1
critThreshold=2

if [[ $errno -eq 0 ]]
then
	opts=$(getopt \
		--name "$cmd" \
		--options "w:c:" \
		--longoptions "warning:,critical:" \
		-- "$@"
	)
	if [[ $? -ne 0 ]]
	then
		errno=1
	fi
fi

if [[ $errno -eq 0 ]]
then
	moreopt=0
	while [[ $moreopt -eq 0 ]]
	do
		case "$1" in
			-w | --warning ) warnThreshold=$2; shift 2;;
			-c | --critical ) critThreshold=$2; shift 2;;
			-- ) moreopt=1; shift 1;;
			* ) moreopt=1;;
		esac
	done

	warnThreshold=$(lowest $warnThreshold $critThreshold)
	critThreshold=$(greatest $warnThreshold $critThreshold)

	eval set -- ${opts##*--}
	shift $((OPTIND-1))
fi

if [[ $errno -eq 0 ]]
then
	main	
	errno=$?
fi

exit $errno
