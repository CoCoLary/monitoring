#!/bin/python3

import sys
import argparse
import locale
from time import time, localtime 
from datetime import datetime
import http.client
import ssl
from OpenSSL import crypto

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

def verbose( text, level = 1, **kwargs ) :
	if "args" in vars() or "args" in globals() and args.verbose and args.verbose >= int( level ) :
		if kwargs :
			print( text, **kwargs )
		else :
			print( text )

def checkHTTP( hostname ) :
	vexit = OK
	output = ""
	perfdata = ""

	isHTTP = True

	# Resquesting HTTPS
	if vexit == OK :
		t0 = time()
		conn = http.client.HTTPConnection( hostname, timeout = args.critical )
		try :
			conn.request( "GET", "/" )
			response = conn.getresponse()
		except ConnectionRefusedError :
			isHTTPS = False
		finally :
			conn.close()
		t1 = time()
		delta = t1 - t0

		# Checking response status
		if isHTTP :
			if response.status < 300 or 400 <= response.status :
				vexit = max( vexit, WARNING )

		# Checking response time
		if delta >= args.warning :
			if delta >= args.critical :
				vexit = max( vexit, CRITICAL )
			else :
				vexit = max( vexit, WARNING )

	if isHTTP :
		output = "HTTP : %s %s in %.3f seconds" % ( response.status, response.reason, delta )
		if vexit == WARNING :
			output += " Pas de redirection vers HTTPS"
	else :
		output = "HTTP connexion refusée"
		vexit = max( vexit, CRITICAL )
	verbose( output )
	perfdata = "http=%.3fs;%d;%d;%d;%d" % ( delta, args.warning, args.critical, 0, args.critical )
	verbose( perfdata )

	return vexit, output, perfdata

def checkHTTPS( hostname ) :
	vexit = OK
	output = ""
	perfdata = ""

	isHTTPS = True

	# Resquesting HTTPS
	if vexit == OK :
		t0 = time()
		conn = http.client.HTTPSConnection( hostname, timeout = args.critical )
		try :
			conn.request( "GET", "/" )
			response = conn.getresponse()
		except ConnectionRefusedError :
			isHTTPS = False
		finally :
			conn.close()
		t1 = time()
		delta = t1 - t0

		# Checking response status
		if isHTTPS :
			if response.status < 200 or 300 <= response.status :
				vexit = max( vexit, CRITICAL )

		# Checking response time
		if delta >= args.warning :
			if delta >= args.critical :
				vexit = max( vexit, CRITICAL )
			else :
				vexit = max( vexit, WARNING )

	if isHTTPS :
		output = "HTTPS : %s %s in %.3f seconds" % ( response.status, response.reason, delta )
	else :
		output = "HTTPS connexion refusée"
		vexit = max( vexit, CRITICAL )
	verbose( output )
	perfdata = "https=%.3fs;%d;%d;%d;%d" % ( delta, args.warning, args.critical, 0, args.critical )
	verbose( perfdata )

	return vexit, output, perfdata

def checkCert( hostname ) :
	vexit = OK
	output = ""
	perfdata = ""

	isHTTPS = True

	# Getting certificate
	try :
		certPEM = ssl.get_server_certificate(( hostname, 443 ))
	except ConnectionRefusedError :
		isHTTPS = False

	if isHTTPS :
		# Extracting informations
		x509 = crypto.load_certificate( crypto.FILETYPE_PEM, certPEM )
		subject = x509.get_subject()
		cn = subject.CN
		notBefore = datetime.strptime( x509.get_notBefore().decode(), "%Y%m%d%H%M%SZ" )
		notAfter = datetime.strptime( x509.get_notAfter().decode(), "%Y%m%d%H%M%SZ" )

		# Check expiration
		now = datetime.now()
		certDuration = notAfter - notBefore
		certRemaining = notAfter - now

		if certRemaining < certDuration / 3 :
			if now > notAfter :
				vexit = max( vexit, CRITICAL )
			else :
				vexit = max( vexit, WARNING )

	if isHTTPS :
		output = "Certificat %s :\n\tde %s\n\t à %s" % ( cn, notBefore, notAfter )
	else :
		output = "Pas de certificat"

	return vexit, output, perfdata

if __name__ == "__main__" :
	# ARGUMENTS
	argParser = argparse.ArgumentParser()
	
	# Defining arguments
	argParser.add_argument( "-v", "--verbose", action = "count", default = 0, help = "Verbose level. Can be used several times" )
	argParser.add_argument( "-w", "--warning", type = int, default = 5, help = "Response time in second, waring threshold" )
	argParser.add_argument( "-c", "--critical", type = int, default = 10, help = "Response time in second, critical threshold" )
	argParser.add_argument( "hostname", type = str, help = "Hostname" )
	
	# Getting args
	args = argParser.parse_args()
	verbose( args )

	# MAIN
	vexit = OK
	output = ""
	perfdata = ""

	locale.setlocale( locale.LC_TIME, "fr_FR.UTF-8" )

	# Check HTTP
	vret, outputret, perfdataret = checkHTTP( args.hostname )
	vexit = max( vexit, vret )
	output += "\n%s" % outputret
	perfdata += " %s" % perfdataret

	# Check HTTPS
	vret, outputret, perfdataret = checkHTTPS( args.hostname )
	vexit = max( vexit, vret )
	output += "\n%s" % outputret
	perfdata += " %s" % perfdataret

	# Check Cert
	vret, outputret, perfdataret = checkCert( args.hostname )
	vexit = max( vexit, vret )
	output += "\n%s" % outputret
	perfdata += " %s" % perfdataret
	
	if vexit == OK :
		status = "OK"
	elif vexit == WARNING :
		status = "WARNING"
	elif vexit == CRITICAL :
		status = "CRITICAL"
	else :
		status = "UNKNOWN"

	print( "%s - %s|%s" % ( status, output, perfdata ))
	sys.exit( vexit )
